import requests
import typer  # Библиотека для реализации CLI интерфейса.
import sys


class URLDatabase:
    """Абстрактный класс родитель. Вмещает в себя базовые URL с параметрами по умолчанию."""
    url_city_info = "http://api.geonames.org/searchJSON?&cities=cities15000&username=Septefril"
    url_currencyCode = "http://api.geonames.org/countryInfoJSON?&username=Septefril"


class CityInform(URLDatabase):
    """Класс в котором будет проходить вся работа"""
    valid_city_request = ''
    city_parameter = {'name_equals': ''}  # Значения (город) присваивается пользователем через CLI при запуске файла.

    """При запуске файла через CLI, метод __init__ заменяет значение атрибута city_parameter
    на введенный пользователем. После этого идёт запуск метода result_of_the_class внутри класса.
    Magic метод __init__ даёт возможность классу, быть вызванным как функция, что удобно при использовании CLI typer."""
    def __init__(self, param):
        self.city_parameter['name_equals'] = param
        self.result_of_the_class()

    """Проверка на соединение по базовому запросу с которым будем работать."""
    def check_request(self):
        try:
            city_request = requests.get(self.url_city_info, params=self.city_parameter, timeout=5)
            city_request = city_request.json()
        except Exception:
            print('System Error')
        else:
            self.valid_city_request = city_request

    """Проверка на наличие города в БД geonames.org. Управляющая конструкция if сравнивает значение ключа
     totalResultsCount (это кол-во результатов поиска, которые соответствуют запросу) с нулём."""
    def validating_user_input(self):
        if self.valid_city_request['totalResultsCount'] == 0:
            raise SystemError(f'Invalid city name: {self.city_parameter["name_equals"]}')
        else:
            pass

    """"Метод запускаемый в методе data_processing. Получает параметр в виде кода страны, выполняет запрос
     на информацию о стране и возвращает валюту в виде строки."""
    def definition_of_currency(self, code):
        currency_parameter = {'country': code}
        try:
            currency_request = requests.get(self.url_currencyCode, params=currency_parameter, timeout=5)
            currency_request = currency_request.json()
        except Exception:
            print('System Error')
        else:
            return print(currency_request['geonames'][0]['currencyCode'])

    """В этом методе обрабатываются полученные данные. Циклом прогоняются результаты запроса, находятся те, которые
    имеют статус города (значение 'city, village,...') и формируется результат поиска со всей необходимой
    информацией (включая валюту, которую запрашивает и возвращает метод definition_of_currency)"""
    def data_processing(self):
        for place in self.valid_city_request['geonames']:
            for key, value in place.items():
                if value == 'city, village,...':
                    print('-' * 50)
                    print(place['name'], '\n')
                    print(place['countryName'])
                    self.definition_of_currency(place['countryCode'])
                    print(place['population'])
                    print('=' * 50)

    """Метод, который запускает все необходимые функции."""
    def result_of_the_class(self):
        self.check_request()
        self.validating_user_input()
        self.data_processing()


if __name__ == "__main__":
    sys.tracebacklimit = 0  # Отключает traceback ошибок. При дебагинге строку можно закомментить.
    typer.run(CityInform)  # Запуск CLI
